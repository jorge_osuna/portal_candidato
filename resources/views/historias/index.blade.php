@extends('layouts.app')
@section('content')
@foreach ($datos['imagen_historias'] as $imagen)
    @if ($imagen->orden_imagen==1)
        <img class="img-banner"  width="100%" height="320px" src="{{ $imagen->ruta_imagen }}" alt="">
    @endif
@endforeach
<br>
<hr>
<div class="container">
    @foreach ($datos['historias'] as $historia)
        <div class="con-titulo">
            <h1 class="tuclase">{{ $historia->titulo }}</h1>
        </div>
        <br>
        <hr>
        <br>
    @endforeach
</div>
<div class="cont-todo">
    <div class="cont">
        <div class="contenedor-texto">
            @foreach ($datos['historias'] as $historia)
                <p style="line-height: calc(1em + 3px);">{{ $historia->descripcion }}</p>
            @endforeach
        </div>
        <div class="contenedor-imagen">
            @foreach ($datos['imagen_historias'] as $imagen)
                @if ($imagen->orden_imagen>1)
                    <img class="img-descrp" src="{{ $imagen->ruta_imagen }}" alt=""><br>
                @endif
            @endforeach
        </div>
    </div>
</div>
@endsection
