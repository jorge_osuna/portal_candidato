@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="container">
        @foreach ($datos['noticias'] as $notica)
            <div class="card" style="width: 18rem;">
                @foreach ($datos['img_notica'] as $img_notica)
                    @if ( $img_notica->order==1)
                        <img src="{{ $img_notica->ruta_imagen }}" class="card-img-top" alt="...">
                    @endif
                @endforeach
                <div class="card-body">
                <h5 class="card-title">{{ $notica->titulo }}</h5>
                <p class="card-text">{{ substr($notica->descripcion,0,50) }}</p>
                <a href="{{ route('noticias.show',$notica->id )}}" class="btn btn-primary">{{ $notica->fecha_noticia }}</a>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
