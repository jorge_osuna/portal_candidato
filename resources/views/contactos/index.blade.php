@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="container">
        <form action="{{ route('contactos.store') }}" method="post">
            @csrf
            <div class="input-group">
                <label for="nombre">Nombre :
                    <input type="text" name="nombre" id="nombre" class="form-control" placeholder="nombre">
                </label>
            </div>
            <div class="input-group">
                <label for="correo">Correo :
                    <input type="email" name="correo" id="correo" class="form-control" placeholder="correo">
                </label>
            </div>
            <div class="input-group">
                <label for="asunto">Asunto :
                    <input type="text" name="asunto" id="asunto" class="form-control" placeholder="asunto">
                </label>
            </div>
            <div class="form-group">
              <label for="descripcion">Descripcion :</label>
              <textarea class="form-control" name="descripcion" id="descripcion" rows="9"></textarea>
            </div>
            <button type="submit" class="btn btn-info">Enviar</button>
        </form>
    </div>
</div>
@endsection
