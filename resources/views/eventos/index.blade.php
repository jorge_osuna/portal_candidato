@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="container">
        @foreach ($datos['eventos'] as $eventos)
            <div class="card" style="width: 18rem;">
                @foreach ($datos['img_eventos'] as $img_eventos)
                    @if ( $img_eventos->order==1)
                        <img src="{{ $img_eventos->ruta_imagen }}" class="card-img-top" alt="...">
                    @endif
                @endforeach
                <div class="card-body">
                <h5 class="card-title">{{ $eventos->titulo }}</h5>
                <p class="card-text">{{ substr($eventos->descripcion,0,50) }}</p>
                <a href="{{ route('noticias.show',$eventos->id )}}" class="btn btn-primary">{{ $eventos->fecha_evento }}</a>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
