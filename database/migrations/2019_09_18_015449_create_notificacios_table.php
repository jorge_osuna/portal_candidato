<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificaciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificacios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('url_seccion');
            $table->bigInteger('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('users');
            $table->bigInteger('noticias_id')->unsigned()->nullable();
            $table->foreign('noticias_id')->references('id')->on('noticas');
            $table->bigInteger('eventos_id')->unsigned()->nullable();
            $table->foreign('eventos_id')->references('id')->on('eventos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificacios');
    }
}
