<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagenNoticiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagen__noticias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ruta_imagen',150);
            $table->bigInteger('noticia_id')->unsigned();
            $table->integer('order');
            $table->foreign('noticia_id')->references('id')->on('noticas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagen__noticias');
    }
}
