<?php

namespace App;
use App\Notica;
use Illuminate\Database\Eloquent\Model;

class Imagen_Noticia extends Model
{
    //
    protected $fillable = [
        'ruta_imagen','noticia_id','order',
    ];

    public function noticia(){
        return $this->belongsTo(Notica::class);
    }
}
