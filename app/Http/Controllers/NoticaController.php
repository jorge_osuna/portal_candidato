<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notica;
use App\Imagen_Noticia;

class NoticaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $noticias=Notica::all();
        $img_notica=Imagen_Noticia::all();

        $datos=[
            'noticias'=>$noticias,
            'img_notica'=>$img_notica
        ];
        return view('noticias.index',compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'titulo'=>'required',
            'descripcion'=>'required',
            'fecha_noticia'=>'required'
        ]);
        $carpeta = 'img/noticia/'.$_POST['titulo'];
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
        }
        $noticia=Notica::create($request->except('_token'));
        $noticia->save();
        return redirect('/noticias');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $noticias=Notica::find($id);
        $img_noticias=Imagen_Noticia::all()->where('noticia_id',$id);
        $datos=[
            "noticias"=>$noticias,
            "img_noticias"=>$img_noticias,
        ];
        return view('noticias.show',compact('datos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $noticia=Notica::find($id);
        $img_noticas=Imagen_Noticia::all()->where('noticia_id',$id);
        $directorio="img/noticia/".$noticia->titulo;
        foreach(glob($directorio."/*") as $elementos){
            if (is_dir($elementos)) {
                    EliminarDir($elementos);
            }
            else{
                unlink($elementos);
            }
        }
        rmdir($directorio);
        foreach ($img_noticas as $img_noticia) {
            $img_noticia->delete();
        }
        $noticia->delete();
        return view('home');

    }
}
