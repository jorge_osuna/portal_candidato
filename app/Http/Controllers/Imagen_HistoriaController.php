<?php

namespace App\Http\Controllers;
use App\Imagen_Historia;
use Illuminate\Http\Request;

class Imagen_HistoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //echo "entro la imagen";
        //exit();
        //$request->validate([
        //    'ruta_imagen'=>'required|image|mimes:jpg,png,jpeg',
        //    'orden'=>'required'
       // ]);
       // echo "paso el request";
       // exit();
        $carpeta = 'img/historia';
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
        }

        $imageName=request()->ruta_imagen->getClientOriginalName();
        request()->ruta_imagen->move(public_path($carpeta),$imageName);

        $imagen_historia = Imagen_Historia::create($request->except('_token'));
        $imagen_historia->ruta_imagen=$carpeta."/".request()->ruta_imagen->getClientOriginalName();
        $imagen_historia->save();
        return redirect('/historias');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $imagen=Imagen_Historia::find($id);
        //echo "esto se va a borrar ".$imagen->ruta_imagen;
        //exit();
        unlink($imagen->ruta_imagen);
        $imagen->delete();

        return redirect('home');
    }
}
