<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Evento;
use App\Imagen_Evento;
class EventoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $eventos=Evento::all();
        $img_eventos=Imagen_Evento::all();

        $datos=[
            'eventos'=>$eventos,
            'img_eventos'=>$img_eventos
        ];
        return view('eventos.index',compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'titulo'=>'required',
            'descripcion'=>'required',
            'fecha_evento'=>'required'
        ]);
        $carpeta = 'img/evento/'.$_POST['titulo'];
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
        }
        $evento=Evento::create($request->except('_token'));
        $evento->save();
        return redirect('/eventos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $evento=Evento::find($id);
        $img_eventos=Imagen_Evento::all()->where('evento_id',$id);
        $datos=[
            "evento"=>$evento,
            "img_eventos"=>$img_eventos,
        ];
        return view('eventos.show',compact('datos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $evento=Evento::find($id);
        $img_eventos=Imagen_Evento::all()->where('evento_id',$id);
        $directorio="img/evento/".$evento->titulo;
        //echo" esta es la carpeta  ".$carpeta;
        //exit();
       foreach(glob($directorio."/*") as $elementos){
         if (is_dir($elementos)) {
                    EliminarDir($elementos);
            }
            else{
                unlink($elementos);
            }
        }
        rmdir($directorio);
        foreach ($img_eventos as $img_evento) {
            $img_evento->delete();
        }
        $evento->delete();
        return view('home');
    }
}
