<?php

namespace App;
use App\Historia;
use Illuminate\Database\Eloquent\Model;

class Imagen_Historia extends Model
{
    //
    protected $fillable = [
        'ruta_imagen','orden_imagen',
    ];

    public function historia(){
        return $this->belongsTo(Historia::class);
    }
}
