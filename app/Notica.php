<?php

namespace App;
use App\Imagen_Noticia;
use Illuminate\Database\Eloquent\Model;

class Notica extends Model
{
    //
    protected $fillable = [
        'titulo','descripcion','fecha_noticia',
    ];

    public function imagen_noticias(){
        return $this->hasMany(Imagen_Noticia::class);
    }
}
