<?php

namespace App;
use App\Evento;
use Illuminate\Database\Eloquent\Model;

class Imagen_Evento extends Model
{
    //
    protected $fillable = [
        'ruta_imagen','evento_id','order',
    ];

    public function evento(){
        return $this->belongsTo(Evento::class);
    }
}
