<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/historias', 'HistoriaController',['except'=>['destroy','show']]);
Route::resource('/administradores', 'AdministradorController',['except'=>['destroy','show','create','edit','store']]);
Route::resource('/imagen_historia', 'Imagen_HistoriaController');
Route::resource('/noticias', 'NoticaController');
Route::resource('/imagen_noticias', 'Imagen_NoticiasController');
Route::resource('/eventos', 'EventoController');
Route::resource('/imagen_eventos', 'Imagen_EventosController');
Route::resource('/contactos', 'ContactoController');
Route::resource('user', 'UserController');

